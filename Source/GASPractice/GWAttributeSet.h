// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"

#include "AbilitySystemComponent.h"

#include "GWAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)


/**
 * 
 */
UCLASS()
class GASPRACTICE_API UGWAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:

	UGWAttributeSet();

	void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty> & OutLifetimeProps) const override;

	UFUNCTION()
	void OnRep_Health(const FGameplayAttributeData& OldData);

	UPROPERTY(ReplicatedUsing = OnRep_Health)
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UGWAttributeSet, Health);

	UPROPERTY()
	FGameplayAttributeData Stamina;
	ATTRIBUTE_ACCESSORS(UGWAttributeSet, Stamina);

	UPROPERTY()
	FGameplayAttributeData StaminaRegen;
	ATTRIBUTE_ACCESSORS(UGWAttributeSet, StaminaRegen);

	UPROPERTY()
	FGameplayAttributeData MovementSpeed;
	ATTRIBUTE_ACCESSORS(UGWAttributeSet, MovementSpeed);
	
};
