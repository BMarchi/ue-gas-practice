// Fill out your copyright notice in the Description page of Project Settings.


#include "GWAttributeSet.h"

// UE Includes
#include "Net/UnrealNetwork.h"

UGWAttributeSet::UGWAttributeSet()
{
	InitHealth(50.0f);
	InitStamina(100.0f);
	InitMovementSpeed(600.0f);
	InitStaminaRegen(0.0f);
}

void UGWAttributeSet::GetLifetimeReplicatedProps(
	TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UGWAttributeSet, Health);
}


void UGWAttributeSet::OnRep_Health(const FGameplayAttributeData& OldData)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGWAttributeSet, Health, OldData);
}
