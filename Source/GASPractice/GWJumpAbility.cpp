// Fill out your copyright notice in the Description page of Project Settings.


#include "GWJumpAbility.h"

// UE Includes
#include "GameFramework/Character.h"

UGWJumpAbility::UGWJumpAbility()
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::NonInstanced;
}

bool UGWJumpAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayTagContainer* SourceTags,
	const FGameplayTagContainer* TargetTags,
	OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	const ACharacter* OurCharacter = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
	return Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags)
		&& OurCharacter && OurCharacter->CanJump();
}

void UGWJumpAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		ACharacter* OurCharacter = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
		OurCharacter->Jump();
		constexpr bool bReplicateEndAbility = true;
		constexpr bool bAbilityCancelled = false;
		EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bAbilityCancelled);
	}
}
