// Fill out your copyright notice in the Description page of Project Settings.


#include "GWGameInstance.h"

// UE Includes
#include "AbilitySystemGlobals.h"

void UGWGameInstance::Init()
{
	Super::Init();

	UAbilitySystemGlobals::Get().InitGlobalData();
}
