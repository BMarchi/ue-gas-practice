// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "GWChargedJump.generated.h"

class UAbilityTask_WaitInputRelease;

/**
 * 
 */
UCLASS()
class GASPRACTICE_API UGWChargedJump : public UGameplayAbility
{
	GENERATED_BODY()

public:

	UGWChargedJump();

	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		const FGameplayEventData* TriggerEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		bool bReplicateEndAbility, bool bWasCancelled) override;

protected:

	UPROPERTY(EditDefaultsOnly)
	float MaxTimeElapsed = 5.0f;
	UPROPERTY(EditDefaultsOnly)
	float MaxExtraVelocityPerSecond = 100.0f;

private:

	UFUNCTION()
	void OnInputReleased(float TimeElapsed);

	UPROPERTY(Transient)
	UAbilityTask_WaitInputRelease* WaitInputReleaseTask = nullptr;
	
};
