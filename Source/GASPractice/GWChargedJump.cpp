// Fill out your copyright notice in the Description page of Project Settings.


#include "GWChargedJump.h"

#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"

UGWChargedJump::UGWChargedJump()
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

void UGWChargedJump::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}
		WaitInputReleaseTask = UAbilityTask_WaitInputRelease::WaitInputRelease(this, false);
		WaitInputReleaseTask->OnRelease.AddDynamic(this, &UGWChargedJump::OnInputReleased);
		WaitInputReleaseTask->ReadyForActivation();
	}
}

void UGWChargedJump::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	if (WaitInputReleaseTask)
	{
		WaitInputReleaseTask->OnRelease.RemoveDynamic(this, &UGWChargedJump::OnInputReleased);
		WaitInputReleaseTask->EndTask();
		WaitInputReleaseTask = nullptr;
	}
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGWChargedJump::OnInputReleased(float TimeElapsed)
{
	ACharacter* OurCharacter = Cast<ACharacter>(GetAvatarActorFromActorInfo());
	if (OurCharacter && !OurCharacter->CanJump())
	{
		CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true);
		return;
	}
	const float ClampedTimeElapsed = FMath::Clamp(TimeElapsed, 0.0f, MaxTimeElapsed);
	OurCharacter->GetCharacterMovement()->JumpZVelocity += MaxExtraVelocityPerSecond * (ClampedTimeElapsed);
	OurCharacter->Jump();
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}
