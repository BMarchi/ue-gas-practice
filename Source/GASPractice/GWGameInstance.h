// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GWGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GASPRACTICE_API UGWGameInstance : public UGameInstance
{
	GENERATED_BODY()

protected:

	void Init() override;
	
};
